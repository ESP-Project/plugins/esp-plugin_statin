#copyright 2023 Commonwealth Informatics
#Author: Bob Zambarano
#contact: bzambarano@commoninf.com


from django.db import connection
from ESP.utils import log
from ESP.settings import DATABASES

# temporary table syntax (TT)
if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc': # ms-sql server
#update to mssql
    TT_SYNTX = "#"
    TT_PRFX = "#"
    concat_operator = '+'
    LIKE_SYNTX = 'like'
    
else: # postgres
    TT_SYNTX = "temporary "
    TT_PRFX = ""
    concat_operator = '||'
    LIKE_SYNTX = 'ilike'

def make_timedelta_parms(cursor):
    cursor.execute(f"""select 365 as dx_prevalence, 365 as statin_prevalence, 210 as statin_avg_period, 183 as pregnancy_prevalence, 75 as max_diabetes_age 
                          into {TT_SYNTX}temp_timedelta_parms""")

def generate_statin_events(uri):
    with connection.cursor() as cursor:
        #make make statin rx events
        counts = 0
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
          cursor.execute(f"""select distinct p.id, p.patient_id, p.provider_id, p.date
                        into {TT_SYNTX}temp_atorvastatins
                         from emr_prescription p
                         cross join (VALUES ('atorvastatin'), ('lipitor')) as atorva(drugs)
                         where p.name like '%' + atorva.drugs + '%'
                         and not exists (select null 
                                         from hef_event t00 
                                         where p.id=t00.object_id and t00.name='rx:atorvastatin')""")
        else:
          cursor.execute(f"""with atorva as (select unnest(array['atorvastatin','lipitor']) as drugs)
                         select distinct id, patient_id, provider_id, date
                         into {TT_SYNTX}temp_atorvastatins
                         from emr_prescription p join atorva d on p.name ilike '%'||d.drugs||'%'
                         where not exists (select null 
                                           from hef_event t00 
                                           where p.id=t00.object_id and t00.name='rx:atorvastatin')""")
        # Common SQL for both databases for inserting data
        cursor.execute(f"""insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
                   select 'rx:atorvastatin' as name, '{uri}' as source, t0."date", current_timestamp as "timestamp",
                        'raw sql method' as note, t0.id as object_id, t1.id as content_type_id, t0.patient_id, t0.provider_id
                   from {TT_PRFX}temp_atorvastatins t0 
                   join django_content_type t1 on t1.app_label = 'emr' and t1.model = 'prescription'""")
        counts += cursor.rowcount

        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""select distinct p.id, p.patient_id, p.provider_id, p.date
                               into {TT_SYNTX}temp_fluvastatins
                               from emr_prescription p
                               cross join (VALUES ('fluvastatin'), ('lescol')) as fluva(drugs)
                               where p.name like '%' + fluva.drugs + '%'
                               and not exists (select null 
                                               from hef_event t00 
                                               where p.id=t00.object_id and t00.name='rx:fluvastatin')""")
        else:
            cursor.execute(f"""with fluva as (select unnest(array['fluvastatin', 'lescol']) as drugs)
                               select distinct id, patient_id, provider_id, date
                               into {TT_SYNTX}temp_fluvastatins
                               from emr_prescription p join fluva d on p.name ilike '%' {concat_operator} d.drugs {concat_operator} '%'
                               where not exists (select null 
                                                 from hef_event t00 
                                                 where p.id=t00.object_id and t00.name='rx:fluvastatin')""")
        # Common SQL for both databases for inserting data
        cursor.execute(f"""insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
                           select 'rx:fluvastatin' as name, '{uri}' as source, t0."date", current_timestamp as "timestamp",
                                'raw sql method' as note, t0.id as object_id, t1.id as content_type_id, t0.patient_id, t0.provider_id
                           from {TT_PRFX}temp_fluvastatins t0 join django_content_type t1 on t1.app_label='emr' and t1.model='prescription'""")
        counts += cursor.rowcount


        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""select distinct p.id, p.patient_id, p.provider_id, p.date
                               into {TT_SYNTX}temp_lovastatins
                               from emr_prescription p
                               cross join (VALUES ('lovastatin'), ('mevinolin'), ('mevacor'), ('altoprev')) as lova(drugs)
                               where p.name like '%' + lova.drugs + '%'
                               and not exists (select null 
                                               from hef_event t00 
                                               where p.id=t00.object_id and t00.name='rx:lovastatin')""")
        else:
            cursor.execute(f"""with lova as (select unnest(array['lovastatin', 'mevinolin', 'mevacor', 'altoprev']) as drugs)
                               select distinct id, patient_id, provider_id, date
                               into {TT_SYNTX}temp_lovastatins
                               from emr_prescription p join lova d on p.name ilike '%' {concat_operator} d.drugs {concat_operator} '%'
                               where not exists (select null 
                                                 from hef_event t00 
                                                 where p.id=t00.object_id and t00.name='rx:lovastatin')""")
        # Common SQL for both databases for inserting data
        cursor.execute(f"""insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
                           select 'rx:lovastatin' as name, '{uri}' as source, t0."date", current_timestamp as "timestamp",
                                  'raw sql method' as note, t0.id as object_id, t1.id as content_type_id, t0.patient_id, t0.provider_id
                           from {TT_PRFX}temp_lovastatins t0 join django_content_type t1 on t1.app_label='emr' and t1.model='prescription'""")
        counts += cursor.rowcount

        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            # Microsoft SQL Server-specific SQL
            cursor.execute(f"""select distinct p.id, p.patient_id, p.provider_id, p.date, p.name
                               into {TT_SYNTX}temp_pitavastatins
                               from emr_prescription p
                               cross join (VALUES ('pitavastatin'), ('livalo'), ('zypitamag'), ('nikita')) as pita(drugs)
                               where p.name like '%' + pita.drugs + '%'
                               and not exists (select null 
                                               from hef_event t00 
                                               where p.id=t00.object_id and t00.name='rx:pitavastatin')""")
        else:
            # PostgreSQL-specific SQL
            cursor.execute(f"""with pita as (select unnest(array['pitavastatin', 'livalo', 'zypitamag', 'nikita']) as drugs)
                               select distinct id, patient_id, provider_id, date, name
                               into {TT_SYNTX}temp_pitavastatins
                               from emr_prescription p join pita d on p.name ilike '%' {concat_operator} d.drugs {concat_operator} '%'
                               where not exists (select null 
                                                 from hef_event t00 
                                                 where p.id=t00.object_id and t00.name='rx:pitavastatin')""")

        # Common SQL for both databases for inserting data
        cursor.execute(f"""insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
                           select 'rx:pitavastatin' as name, '{uri}' as source, t0."date", current_timestamp as "timestamp",
                                'raw sql method' as note, t0.id as object_id, t1.id as content_type_id, t0.patient_id, t0.provider_id
                           from {TT_PRFX}temp_pitavastatins t0 join django_content_type t1 on t1.app_label='emr' and t1.model='prescription'""")
        counts += cursor.rowcount


        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            # Microsoft SQL Server-specific SQL for pravastatin
            cursor.execute(f"""select distinct p.id, p.patient_id, p.provider_id, p.date
                               into {TT_SYNTX}temp_pravastatins
                               from emr_prescription p
                               cross join (VALUES ('pravastatin'), ('pravachol')) as prava(drugs)
                               where p.name like '%' + prava.drugs + '%'
                               and not exists (select null 
                                               from hef_event t00 
                                               where p.id=t00.object_id and t00.name='rx:pravastatin')""")
        else:
            # PostgreSQL-specific SQL for pravastatin
            cursor.execute(f"""with prava as (select unnest(array['pravastatin', 'pravachol']) as drugs)
                               select distinct id, patient_id, provider_id, date
                               into {TT_SYNTX}temp_pravastatins
                               from emr_prescription p join prava d on p.name ilike '%' {concat_operator} d.drugs {concat_operator} '%'
                               where not exists (select null 
                                                 from hef_event t00 
                                                 where p.id=t00.object_id and t00.name='rx:pravastatin')""")
        # Common SQL for both databases for inserting data
        cursor.execute(f"""insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
                           select 'rx:pravastatin' as name, '{uri}' as source, t0."date", current_timestamp as "timestamp",
                                'raw sql method' as note, t0.id as object_id, t1.id as content_type_id, t0.patient_id, t0.provider_id
                           from {TT_PRFX}temp_pravastatins t0 join django_content_type t1 on t1.app_label='emr' and t1.model='prescription'""")
        counts += cursor.rowcount

        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            # Microsoft SQL Server-specific SQL for simvastatin
            cursor.execute(f"""select distinct p.id, p.patient_id, p.provider_id, p.date
                               into {TT_SYNTX}temp_simvastatins
                               from emr_prescription p
                               cross join (VALUES ('simvastatin'), ('zocor')) as simva(drugs)
                               where p.name like '%' + simva.drugs + '%'
                               and not exists (select null 
                                               from hef_event t00 
                                               where p.id=t00.object_id and t00.name='rx:simvastatin')""")
        else:
            # PostgreSQL-specific SQL for simvastatin
            cursor.execute(f"""with simva as (select unnest(array['simvastatin', 'zocor']) as drugs)
                               select distinct id, patient_id, provider_id, date
                               into {TT_SYNTX}temp_simvastatins
                               from emr_prescription p join simva d on p.name ilike '%' {concat_operator} d.drugs {concat_operator} '%'
                               where not exists (select null 
                                                 from hef_event t00 
                                                 where p.id=t00.object_id and t00.name='rx:simvastatin')""")
        # Common SQL for both databases for inserting data
        cursor.execute(f"""insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
                           select 'rx:simvastatin' as name, '{uri}' as source, t0."date", current_timestamp as "timestamp",
                                'raw sql method' as note, t0.id as object_id, t1.id as content_type_id, t0.patient_id, t0.provider_id
                           from {TT_PRFX}temp_simvastatins t0 join django_content_type t1 on t1.app_label='emr' and t1.model='prescription'""")
        counts += cursor.rowcount

        return counts

def generate_statin_timespans(uri):
    with connection.cursor() as cursor:
        # Make statin rx timespans
        make_timedelta_parms(cursor)
        # Inserting data into temp_statin_rx_high
        cursor.execute(f"""select e.patient_id, e.date, e.object_id, e.id, parm.*
                          into {TT_SYNTX}{TT_PRFX}temp_statin_rx_high
                          from hef_event e
                          join {TT_PRFX}temp_timedelta_parms parm on 1=1
                          where name in ('rx:atorvastatin', 'rx:rosuvastatin')""")

        # Calculating timespans and inserting into temp_statin_rx_high_spans
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            # Microsoft SQL Server-specific SQL
            cursor.execute(f"""select e.*, COALESCE(p.start_date, e.date) as start_date, 
                                case 
                                    when p.end_date is not null then DATEADD(day, e.statin_prevalence, p.end_date)
                                    when p.dose is not null and p.quantity_float is not null and p.dose LIKE '%^[0-9\.]+%' then
                                        DATEADD(day, 
                                                ROUND(e.statin_prevalence + 
                                                      (p.quantity_float / CAST(p.dose AS numeric)) * COALESCE(CAST(p.refills AS numeric), 1)),
                                                COALESCE(p.start_date, e.date))
                                    else DATEADD(day, e.statin_prevalence + e.statin_avg_period, COALESCE(p.start_date, e.date))
                                end as end_date, 'intensity:h' as intensity
                              into {TT_SYNTX}{TT_PRFX}temp_statin_rx_high_spans
                              from {TT_PRFX}temp_statin_rx_high e 
                              join emr_prescription p on p.id=e.object_id""")
        else:
            # PostgreSQL-specific SQL
            cursor.execute(f"""select e.*, COALESCE(p.start_date, e.date) as start_date, 
                                case 
                                    when p.end_date is not null then p.end_date + interval '1 day' * e.statin_prevalence
                                    when p.dose is not null and p.quantity_float is not null and p.dose ~ '^[0-9\\.]+$' then
                                        COALESCE(p.start_date, e.date) + interval '1 day' * 
                                            (ROUND(e.statin_prevalence + 
                                                   (p.quantity_float / p.dose::numeric)::numeric * COALESCE(p.refills::numeric, 1)))
                                    else COALESCE(p.start_date, e.date) + interval '1 day' * (e.statin_prevalence + e.statin_avg_period)
                                end as end_date, 'intensity:h' as intensity
                              into {TT_SYNTX}{TT_PRFX}temp_statin_rx_high_spans
                              from {TT_PRFX}temp_statin_rx_high e 
                              join emr_prescription p on p.id=e.object_id""")
        # Inserting data into temp_statin_rx_nothigh
        cursor.execute(f"""select e.patient_id, e.date, e.object_id, e.id, parm.*
                          into {TT_SYNTX}{TT_PRFX}temp_statin_rx_nothigh
                          from hef_event e
                          join {TT_PRFX}temp_timedelta_parms parm on 1=1
                          where name in ('rx:fluvastatin', 'rx:lovastatin', 'rx:pitavastatin', 'rx:pravastatin', 'rx:simvastatin')""")

        # Calculating timespans and inserting into temp_statin_rx_nothigh_spans
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            # Microsoft SQL Server-specific SQL
            cursor.execute(f"""select e.*, COALESCE(p.start_date, e.date) as start_date, 
                                case 
                                    when p.end_date is not null then DATEADD(day, e.statin_prevalence, p.end_date)
                                    when p.dose is not null and p.quantity_float is not null and p.dose LIKE '%^[0-9\\.]+$%' then
                                        DATEADD(day, 
                                                ROUND(e.statin_prevalence + 
                                                      (p.quantity_float / CAST(p.dose AS numeric)) * COALESCE(CAST(p.refills AS numeric), 1)),
                                                COALESCE(p.start_date, e.date))
                                    else DATEADD(day, e.statin_prevalence + e.statin_avg_period, COALESCE(p.start_date, e.date))
                                end as end_date, 'intensity:m' as intensity
                              into {TT_SYNTX}{TT_PRFX}temp_statin_rx_nothigh_spans
                              from {TT_PRFX}temp_statin_rx_nothigh e 
                              join emr_prescription p on p.id=e.object_id""")
        else:
            # PostgreSQL-specific SQL
            cursor.execute(f"""select e.*, COALESCE(p.start_date, e.date) as start_date, 
                                case 
                                    when p.end_date is not null then p.end_date + interval '1 day' * e.statin_prevalence
                                    when p.dose is not null and p.quantity_float is not null and p.dose ~ '^[0-9\\.]+$' then
                                        COALESCE(p.start_date, e.date) + interval '1 day' * 
                                            (ROUND(e.statin_prevalence + 
                                                   (p.quantity_float / p.dose::numeric)::numeric * COALESCE(p.refills::numeric, 1)))
                                    else COALESCE(p.start_date, e.date) + interval '1 day' * (e.statin_prevalence + e.statin_avg_period)
                                end as end_date, 'intensity:m' as intensity
                              into {TT_SYNTX}{TT_PRFX}temp_statin_rx_nothigh_spans
                              from {TT_PRFX}temp_statin_rx_nothigh e 
                              join emr_prescription p on p.id=e.object_id""")
         # Creating temp_statin_rx_span_events with window functions
        cursor.execute(f"""select t00.*, 
                            sum(case when start_date >= prior_end then 1 else 0 end) over 
                               (partition by patient_id, intensity order by i) as grp
                          into {TT_SYNTX}{TT_PRFX}temp_statin_rx_span_events
                          from (
                              select t0.*, 
                                     lag(end_date) over (partition by patient_id, intensity order by start_date, end_date) as prior_end,
                                     row_number() over (partition by patient_id, intensity order by start_date, end_date, object_id) as i
                              from (
                                  select * from {TT_PRFX}temp_statin_rx_nothigh_spans
                                  union 
                                  select * from {TT_PRFX}temp_statin_rx_high_spans
                              ) t0
                          ) t00""")
        # Aggregating spans based on grouping
        cursor.execute(f"""select patient_id, min(start_date) as start_date, max(end_date) as end_date, intensity, grp
                          into {TT_SYNTX}{TT_PRFX}temp_statin_rx_spans
                          from {TT_PRFX}temp_statin_rx_span_events se
                          group by patient_id, intensity, grp""")
        cursor.execute(f"""select * into {TT_SYNTX}{TT_PRFX}temp_new_statin_rx_spans
                          from {TT_PRFX}temp_statin_rx_spans se
                          where not exists (select null from hef_timespan ht 
                                            where ht.patient_id = se.patient_id 
                                                  and se.start_date = ht.start_date 
                                                  and se.end_date = ht.end_date 
                                                  and ht.name = 'statin')""")

        # Identifying overlapping statin timespans
        cursor.execute(f"""select id into {TT_SYNTX}{TT_PRFX}overlapping_statin_timespans from hef_timespan ht 
                            where exists (select null from {TT_PRFX}temp_new_statin_rx_spans ss
                                        where ht.patient_id = ss.patient_id 
                                                and ((ht.start_date <= ss.start_date and ht.end_date > ss.start_date)
                                                   or (ht.start_date <= ss.end_date and ht.end_date > ss.end_date))
                                        and ht.name = 'statin')""")
        cursor.execute(f"""delete from hef_timespan_events te 
                            where exists (select null from {TT_PRFX}overlapping_statin_timespans st 
                                        where st.id = te.timespan_id)""")
        # Cascading deletes
        cursor.execute(f"""delete from nodis_caseactivehistory cah where cah.case_id in
            (select c.id from nodis_case c join nodis_case_timespans ct on ct.case_id = c.id 
             join {TT_PRFX}overlapping_statin_timespans st on ct.timespan_id = st.id)""")
        cursor.execute(f"""delete from nodis_case_events ce where ce.case_id in
            (select c.id from nodis_case c join nodis_case_timespans ct on ct.case_id = c.id 
             join {TT_PRFX}overlapping_statin_timespans st on ct.timespan_id = st.id)""")
        cursor.execute(f"""delete from nodis_case_timespans ct where ct.timespan_id in
            (select st.id from {TT_PRFX}overlapping_statin_timespans st)""")
        cursor.execute(f"""delete from nodis_case nc where nc.id in
            (select ct.case_id from nodis_case_timespans ct
             join {TT_PRFX}overlapping_statin_timespans st on ct.timespan_id = st.id)""")     
        # Deleting from hef_timespan based on overlapping statin timespans
        cursor.execute(f"""delete from hef_timespan ht
                            where exists (select null from {TT_PRFX}overlapping_statin_timespans st
                                        where st.id = ht.id)""")

        # Inserting new statin timespans into hef_timespan
        cursor.execute(f"""insert into hef_timespan (name, source, patient_id, start_date, end_date, timestamp, pattern)
                            select 'statin' as name, '{uri}' as source, patient_id, start_date, end_date,
                                current_timestamp as timestamp, intensity as pattern
                            from {TT_PRFX}temp_new_statin_rx_spans""")
        counts = cursor.rowcount
        cursor.execute(f"""select id, patient_id, start_date, end_date, pattern
                          into {TT_SYNTX}{TT_PRFX}temp_hef_rx_spans
                          from hef_timespan
                          where name='statin'""")

        # Inserting records into hef_timespan_events
        cursor.execute(f"""insert into hef_timespan_events (event_id, timespan_id)
                          select t0.id as event_id, t1.id as timespan_id
                          from {TT_PRFX}temp_statin_rx_span_events t0
                          join {TT_PRFX}temp_hef_rx_spans t1 on t0.patient_id=t1.patient_id and t0.intensity=t1.pattern
                            and t0.start_date>=t1.start_date and t0.end_date<=t1.end_date
                          where not exists (select null from hef_timespan_events te
                                            where te.event_id=t0.id and te.timespan_id=t1.id)""")
        return counts
   
def generate_liver_events(uri):
    with connection.cursor() as cursor:
        # Make liver dx events
        counts = 0
        # Selecting liver disease events into a temporary table
        cursor.execute(f"""select distinct e.id, e.patient_id, e.provider_id, e.date
                         into {TT_SYNTX}temp_liver_disease
                         from emr_encounter e
                         where not exists (select null 
                                           from hef_event t00 
                                           join django_content_type t01 on t01.app_label = 'emr' and t01.model = 'encounter'
                                           where e.id = t00.object_id and t00.name = 'dx:liver_disease')
                               and exists (select null from emr_encounter_dx_codes t01 where t01.dx_code_id = 'icd10:B17.0' and t01.encounter_id = e.id)""")
        # Inserting liver disease events into hef_event
        cursor.execute(f"""insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
                         select 'dx:liver_disease' as name, '{uri}' as source, t0."date", current_timestamp as "timestamp",
                              'raw sql method' as note, t0.id as object_id, t1.id as content_type_id, t0.patient_id, t0.provider_id
                         from {TT_PRFX}temp_liver_disease t0 join django_content_type t1 on t1.app_label = 'emr' and t1.model = 'encounter'""")
        counts = cursor.rowcount
        return counts
        
def generate_liver_timespans(uri):
    with connection.cursor() as cursor:
        #make make liver timespans
        make_timedelta_parms(cursor)
         # Creating temp_liver_events table
        cursor.execute(f"""select e.patient_id, e.date, e.id
                          into {TT_SYNTX}temp_liver_events
                          from hef_event e
                          where name = 'dx:liver_disease'""")

        # Calculating liver event spans and inserting into temp_liver_event_spans
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            # Microsoft SQL Server-specific SQL
            cursor.execute(f"""select t00.*, 
                                sum(case when start_date >= prior_end then 1 else 0 end) over 
                                   (partition by patient_id order by i) as grp
                              into {TT_SYNTX}temp_liver_event_spans
                              from
                              (select t0.*, lag(end_date) over (partition by patient_id order by start_date, end_date) as prior_end,
                                 row_number() over (partition by patient_id order by start_date, end_date) as i
                              from
                              (select e.patient_id, e.id, e.date as start_date,
                                DATEADD(day, parm.dx_prevalence, e.date) as end_date
                              from {TT_PRFX}temp_timedelta_parms parm
                              join {TT_PRFX}temp_liver_events e on 1=1) t0) t00""")
        else:
            # PostgreSQL-specific SQL
            cursor.execute(f"""select t00.*, 
                                sum(case when start_date >= prior_end then 1 else 0 end) over 
                                   (partition by patient_id order by i) as grp
                              into {TT_SYNTX}temp_liver_event_spans
                              from
                              (select t0.*, lag(end_date) over (partition by patient_id order by start_date, end_date) as prior_end,
                                 row_number() over (partition by patient_id order by start_date, end_date) as i
                              from
                              (select e.patient_id, e.id, e.date as start_date,
                                e.date + interval '1 day' * parm.dx_prevalence as end_date
                              from {TT_PRFX}temp_timedelta_parms parm
                              join {TT_PRFX}temp_liver_events e on 1=1) t0) t00""")
        cursor.execute(f"""select patient_id, min(start_date) as start_date, max(end_date) as end_date, grp
                          into {TT_SYNTX}temp_liver_spans
                          from {TT_PRFX}temp_liver_event_spans
                          group by patient_id, grp""")

        # Inserting liver span data into hef_timespan if it doesn't already exist
        cursor.execute(f"""insert into hef_timespan (name, source, patient_id, start_date, end_date, timestamp, pattern)
                          select 'liver_disease', '{uri}', patient_id, start_date, end_date,
                                 current_timestamp, 'type:dx' as pattern
                          from {TT_PRFX}temp_liver_spans ls 
                          where not exists (select null from hef_timespan ts 
                                            where ts.name = 'liver_disease' 
                                                  and ts.patient_id = ls.patient_id 
                                                  and ts.start_date = ls.start_date 
                                                  and ts.end_date = ls.end_date)""")
        counts = cursor.rowcount
        cursor.execute(f"""select id, patient_id, start_date, end_date, pattern
                          into {TT_SYNTX}temp_liver_span_ids
                          from hef_timespan
                          where name = 'liver_disease'""")

        # Inserting records into hef_timespan_events
        cursor.execute(f"""insert into hef_timespan_events (event_id, timespan_id)
                          select t0.id as event_id, t1.id as timespan_id
                          from {TT_PRFX}temp_liver_event_spans t0
                          join {TT_PRFX}temp_liver_span_ids t1 on t0.patient_id = t1.patient_id 
                            and t0.start_date >= t1.start_date and t0.end_date <= t1.end_date
                          where not exists (select null from hef_timespan_events tse 
                                            where tse.event_id = t0.id and tse.timespan_id = t1.id)""")
        counts = cursor.rowcount

    return counts
        
def generate_rhabdo_events(uri):
    with connection.cursor() as cursor:
         # Selecting distinct rhabdomyolysis events into a temporary table
        cursor.execute(f"""select distinct e.id, e.patient_id, e.provider_id, e.date
                          into {TT_SYNTX}temp_rhabdomyolysis
                          from emr_encounter e
                          where not exists (select null 
                                            from hef_event t00 
                                            join django_content_type t01 on t01.app_label = 'emr' and t01.model = 'encounter'
                                            where e.id = t00.object_id and t00.name = 'dx:rhabdomyolysis')
                                and exists (select null from emr_encounter_dx_codes t01 where t01.dx_code_id = 'icd10:M62.82' and t01.encounter_id = e.id)""")

        # Inserting rhabdomyolysis events into hef_event
        cursor.execute(f"""insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
                          select 'dx:rhabdomyolysis' as name, '{uri}' as source, t0."date", current_timestamp as "timestamp",
                               'raw sql method' as note, t0.id as object_id, t1.id as content_type_id, t0.patient_id, t0.provider_id
                          from {TT_PRFX}temp_rhabdomyolysis t0 join django_content_type t1 on t1.app_label = 'emr' and t1.model = 'encounter'""")
        return cursor.rowcount
        
def generate_rhabdo_timespans(uri):
    with connection.cursor() as cursor:
        #make make rhabdo timespans
        make_timedelta_parms(cursor)
         # Creating temp_rhabdo_events table
        cursor.execute(f"""select e.patient_id, e.date, e.id
                          into {TT_SYNTX}temp_rhabdo_events
                          from hef_event e
                          where name = 'dx:rhabdomyolysis'""")

        # Calculating rhabdo event spans and inserting into temp_rhabdo_span_events
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            # Microsoft SQL Server-specific SQL
            cursor.execute(f"""select t00.*, 
                                sum(case when start_date >= prior_end then 1 else 0 end) over 
                                   (partition by patient_id order by i) as grp
                              into {TT_SYNTX}temp_rhabdo_span_events
                              from
                              (select t0.*, lag(end_date) over (partition by patient_id order by start_date, end_date) as prior_end,
                                 row_number() over (partition by patient_id order by start_date, end_date) as i
                              from
                              (select e.patient_id, e.id, e.date as start_date,
                                DATEADD(day, parm.dx_prevalence, e.date) as end_date
                              from {TT_PRFX}temp_timedelta_parms parm
                              join {TT_PRFX}temp_rhabdo_events e on 1=1) t0) t00""")
        else:
            # PostgreSQL-specific SQL
            cursor.execute(f"""select t00.*, 
                                sum(case when start_date >= prior_end then 1 else 0 end) over 
                                   (partition by patient_id order by i) as grp
                              into {TT_SYNTX}temp_rhabdo_span_events
                              from
                              (select t0.*, lag(end_date) over (partition by patient_id order by start_date, end_date) as prior_end,
                                 row_number() over (partition by patient_id order by start_date, end_date) as i
                              from
                              (select e.patient_id, e.id, e.date as start_date,
                                e.date + interval '1 day' * parm.dx_prevalence as end_date
                              from {TT_PRFX}temp_timedelta_parms parm
                              join {TT_PRFX}temp_rhabdo_events e on 1=1) t0) t00""")
        # Aggregating rhabdo span data into a temporary table
        cursor.execute(f"""select patient_id, min(start_date) as start_date, max(end_date) as end_date, grp
                          into {TT_SYNTX}temp_rhabdo_spans
                          from {TT_PRFX}temp_rhabdo_span_events
                          group by patient_id, grp""")

        # Inserting rhabdo span data into hef_timespan if it doesn't already exist
        cursor.execute(f"""insert into hef_timespan (name, source, patient_id, start_date, end_date, timestamp, pattern)
                          select 'rhabdomyolysis', '{uri}', patient_id, start_date, end_date,
                                 current_timestamp, 'type:dx' as pattern
                          from {TT_PRFX}temp_rhabdo_spans rs 
                          where not exists (select null from hef_timespan ts 
                                            where ts.name = 'rhabdomyolysis' 
                                                  and ts.patient_id = rs.patient_id 
                                                  and ts.start_date = rs.start_date 
                                                  and ts.end_date = rs.end_date)""")
        counts = cursor.rowcount
        cursor.execute(f"""select id, patient_id, start_date, end_date, pattern
                          into {TT_SYNTX}temp_rhabdo_span_ids
                          from hef_timespan
                          where name = 'rhabdomyolysis'""")

        # Inserting records into hef_timespan_events
        cursor.execute(f"""insert into hef_timespan_events (event_id, timespan_id)
                          select t0.id as event_id, t1.id as timespan_id
                          from {TT_PRFX}temp_rhabdo_span_events t0
                          join {TT_PRFX}temp_rhabdo_span_ids t1 on t0.patient_id = t1.patient_id 
                            and t0.start_date >= t1.start_date and t0.end_date <= t1.end_date
                          where not exists (select null from hef_timespan_events tse 
                                            where tse.event_id = t0.id and tse.timespan_id = t1.id)""")
        return counts                            
                            
def generate_lx_hi_chol_events(uri):
    with connection.cursor() as cursor:
        #make high cholesterol lx events
        cursor.execute(f"""select distinct l.id, l.patient_id, l.provider_id, l.date
                          into {TT_SYNTX}temp_highldl
                          from emr_labresult l
                          where not exists (select null 
                                            from hef_event t00 
                                            join django_content_type t01 on t01.app_label = 'emr' and t01.model = 'labresult'
                                            where l.id = t00.object_id and t00.name = 'lx:cholesterol-ldl:threshold:gte:190')
                                and exists (select null from conf_labtestmap t01 
                                            where t01.test_name = 'cholesterol-ldl' and t01.native_code = l.native_code)
                                and l.result_float >= 190""")

        # Inserting high cholesterol events into hef_event
        cursor.execute(f"""insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
                          select 'lx:cholesterol-ldl:threshold:gte:190' as name, '{uri}' as source, t0."date", 
                                    current_timestamp as "timestamp", 'raw sql method' as note, t0.id as object_id, 
                                    t1.id as content_type_id, t0.patient_id, t0.provider_id
                          from {TT_PRFX}temp_highldl t0 join django_content_type t1 
                                    on t1.app_label = 'emr' and t1.model = 'labresult'""")
        return cursor.rowcount

def generate_ascvd_events(uri):
    with connection.cursor() as cursor:
        #make ascvd dx events
        counts= 0
        # Selecting distinct ASCVD events into a temporary table
        cursor.execute(f"""select distinct e.id, e.patient_id, e.provider_id, e.date
                          into {TT_SYNTX}temp_ascvd
                          from emr_encounter e
                          where not exists (select null 
                                            from hef_event t00 
                                            join django_content_type t01 on t01.app_label = 'emr' and t01.model = 'encounter'
                                            where e.id = t00.object_id and t00.name = 'dx:ascvd')
                                and exists (select null from emr_encounter_dx_codes t01 
                                            where (t01.dx_code_id like 'icd10:I25.1%'
                                                   or t01.dx_code_id like 'icd10:I25.71%'
                                                   or t01.dx_code_id like 'icd10:I25.72%'
                                                   or t01.dx_code_id like 'icd10:I25.73%'
                                                   or t01.dx_code_id like 'icd10:I25.75%'
                                                   or t01.dx_code_id like 'icd10:I25.76%'
                                                   or t01.dx_code_id like 'icd10:I25.79%'
                                                   or t01.dx_code_id like 'icd10:I25.81%'
                                                   or t01.dx_code_id like 'icd10:I25.83%'
                                                   or t01.dx_code_id like 'icd10:I70%')
                                            and t01.encounter_id = e.id)""")

        # Inserting ASCVD events into hef_event
        cursor.execute(f"""insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
                          select 'dx:ascvd' as name, '{uri}' as source, t0."date", current_timestamp as "timestamp",
                               'raw sql method' as note, t0.id as object_id, t1.id as content_type_id, t0.patient_id, t0.provider_id
                          from {TT_PRFX}temp_ascvd t0 join django_content_type t1 on t1.app_label = 'emr' and t1.model = 'encounter'""")
        counts += cursor.rowcount
        # Selecting distinct ASCVD events with extended criteria into a temporary table
        cursor.execute(f"""select distinct e.id, e.patient_id, e.provider_id, e.date
                          into {TT_SYNTX}temp_ex_ascvd
                          from emr_encounter e
                          where not exists (select null 
                                            from hef_event t00 
                                            join django_content_type t01 on t01.app_label = 'emr' and t01.model = 'encounter'
                                            where e.id = t00.object_id and t00.name = 'dx:ascvd')
                                and exists (select null from emr_encounter_dx_codes t01 
                                            where (t01.dx_code_id like 'icd10:I25.1%'
                                                   or t01.dx_code_id like 'icd10:E10.5%'
                                                   or t01.dx_code_id like 'icd10:E11.5%'
                                                   or t01.dx_code_id like 'icd10:I25.9%'
                                                   or t01.dx_code_id like 'icd10:I73.9%')
                                            and t01.encounter_id = e.id)""")

        # Inserting extended ASCVD events into hef_event
        cursor.execute(f"""insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
                          select 'dx:ascvd' as name, '{uri}' as source, t0."date", current_timestamp as "timestamp",
                               'raw sql method' as note, t0.id as object_id, t1.id as content_type_id, t0.patient_id, t0.provider_id
                          from {TT_PRFX}temp_ex_ascvd t0 join django_content_type t1 on t1.app_label = 'emr' and t1.model = 'encounter'""")
        counts += cursor.rowcount

    return counts
        
def generate_statin_cases():
    with connection.cursor() as cursor:
        make_timedelta_parms(cursor)
        #make statin cases and histories
        counts= 0
        cursor.execute(f"""select e0.patient_id, e0.date, min(e0.id) as event_id
                          into {TT_SYNTX}tmp_esr_diag
                          from hef_event e0
                          join (Select patient_id, min(date) as date
                          from hef_event
                          where name='dx:endstagerenal'
                          group by patient_id) e1 on e1.date=e0.date and e1.patient_id=e0.patient_id
                          group by e0.patient_id, e0.date""")
        # Creating temp_statin_case_events table with database-specific syntax
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
          cursor.execute(f"""
              select *
              into {TT_SYNTX}temp_statin_case_events
              from (
                select e.id, e.patient_id, e.name, e.date,
                      e.provider_id, ct.id content_type_id, 'ASCVD' as crit
                from hef_event e
                join django_content_type ct on ct.model = 'event' and ct.app_label = 'hef'
                where e.name in ('dx:ascvd')
                
                union
                
                select e.id, e.patient_id, e.name, e.date,
                      e.provider_id, ct.id content_type_id, 'ldl>=190' as crit
                from hef_event e
                join django_content_type ct on ct.model = 'event' and ct.app_label = 'hef'
                join emr_patient p on p.id = e.patient_id 
                where e.name = 'lx:cholesterol-ldl:threshold:gte:190'
                  and DATEDIFF(year, p.date_of_birth, e.date) > 21
                
                union
                
                select c.id, c.patient_id, c.condition as name, c.date,
                      c.provider_id, ct.id content_type_id, 'diabetes' as crit
                from nodis_case c
                join django_content_type ct on ct.model = 'case' and ct.app_label = 'nodis'
                join emr_patient p on p.id = c.patient_id
                where c.condition in ('diabetes:type-1', 'diabetes:type-2')
                  and (
                      DATEDIFF(year, p.date_of_birth, c.date) - CASE
                          WHEN MONTH(p.date_of_birth) > MONTH(c.date)
                              OR (MONTH(p.date_of_birth) = MONTH(c.date) AND DAY(p.date_of_birth) > DAY(c.date))
                          THEN 1 ELSE 0
                      END
                  ) > 21
            ) t0
            where not exists (
                select null from {TT_PRFX}tmp_esr_diag esr
                where esr.patient_id = t0.patient_id and esr.date < t0.date
            )
        """)
        else:
            cursor.execute(f"""
              select *
              into {TT_SYNTX}temp_statin_case_events
              from (
                  select e.id, e.patient_id, e.name, e.date,
                        e.provider_id, ct.id content_type_id, 'ASCVD' as crit
                  from hef_event e
                  join django_content_type ct on ct.model = 'event' and ct.app_label = 'hef'
                  where e.name in ('dx:ascvd')
                  
                  union
                  
                  select e.id, e.patient_id, e.name, e.date,
                        e.provider_id, ct.id content_type_id, 'ldl>=190' as crit
                  from hef_event e
                  join django_content_type ct on ct.model = 'event' and ct.app_label = 'hef'
                  join emr_patient p on p.id = e.patient_id 
                  where e.name = 'lx:cholesterol-ldl:threshold:gte:190'
                    and EXTRACT(YEAR FROM age(e.date, p.date_of_birth)) > 21
                  
                  union
                  
                  select c.id, c.patient_id, c.condition as name, c.date,
                        c.provider_id, ct.id content_type_id, 'diabetes' as crit
                  from nodis_case c
                  join django_content_type ct on ct.model = 'case' and ct.app_label = 'nodis'
                  join emr_patient p on p.id = c.patient_id
                  where c.condition in ('diabetes:type-1', 'diabetes:type-2')
                    and EXTRACT(YEAR FROM age(c.date, p.date_of_birth)) > 21
              ) t0
              where not exists (
                  select null from {TT_PRFX}tmp_esr_diag esr
                  where esr.patient_id = t0.patient_id and esr.date < t0.date
              )
          """)

        # Creating a temporary table for the 75th birthday of patients
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            # Microsoft SQL Server-specific SQL
            cursor.execute(f"""select id as patient_id, DATEADD(year, 75, date_of_birth) as bd75
                              into {TT_SYNTX}tmp_75th_bday 
                              from emr_patient
                              where date_of_birth is not null""")
        else:
            # PostgreSQL-specific SQL
            cursor.execute(f"""select id as patient_id, (date_of_birth + interval '75 years')::date as bd75
                              into {TT_SYNTX}tmp_75th_bday 
                              from emr_patient
                              where date_of_birth is not null""")

        # Creating a temporary table for pregnancy spans
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            # Microsoft SQL Server-specific SQL
            cursor.execute(f"""select ts.patient_id, ts.start_date, 
                          coalesce(CAST(lead(start_date) over (partition by patient_id order by start_date) AS DATETIME),
                                  CAST(ts.end_date AS DATETIME))
                          + DATEADD(day, parm.pregnancy_prevalence, CAST('1900-01-01' AS DATETIME)) as end_date,
                          id as timespan_id 
                  into {TT_SYNTX}temp_preg_spans 
                  from hef_timespan ts
                  join {TT_PRFX}temp_timedelta_parms parm on 1=1
                  where ts.name = 'pregnancy'""")
        else:
            # PostgreSQL-specific SQL
            cursor.execute(f"""select ts.patient_id, ts.start_date, 
                                      coalesce(ts.end_date,
                                              lead(start_date) over (partition by patient_id order by start_date))
                                      + interval '1 day' * parm.pregnancy_prevalence as end_date,
                                      id as timespan_id 
                              into {TT_SYNTX}temp_preg_spans 
                              from hef_timespan ts
                              join {TT_PRFX}temp_timedelta_parms parm on 1=1
                              where ts.name = 'pregnancy'""")
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            #Microsoft SQL Server-specific SQL
            cursor.execute(f"""
    select t0.*,
          case 
            when t75.bd75 is not null or ted.date is not null 
              then (SELECT MIN(date) FROM (VALUES (t75.bd75), (ted.date)) v (date)) 
            when tls.start_date is not null or trs.start_date is not null or tps.start_date is not NULL
              then (SELECT MIN(date) FROM (VALUES (tls.start_date), (trs.start_date), (tps.start_date)) v (date))
            else null
          end as inactive_date,
          case 
            when t75.bd75 is not null or ted.date is not null or tls.start_date is not null 
            or trs.start_date is not null or tps.start_date is not NULL
              then CAST(0 AS BIT)
            else CAST(1 AS BIT)
		  end as isactive
    into {TT_SYNTX}temp_new_statin_cases
    from    
    (select patient_id, CAST('statin' AS varchar(100)) as condition, min(date) as date, string_agg(crit, ',') as criteria,
      CAST('urn:x-esphealth:disease:commonwealth:statin:v1' AS varchar(255)) as source, CAST('AR' AS varchar(32)) as status, CAST(null AS text) as notes,
      CAST(null AS text) as reportables, current_timestamp as created_timestamp, current_timestamp as updated_timestamp, 
      CAST(null AS datetime2) as sent_timestamp, CAST(0 AS BIT) as followup_sent, 1 as provider_id
    from (select patient_id, crit, min(date) as date
          from {TT_PRFX}temp_statin_case_events
          group by patient_id, crit) t00
    group by patient_id) t0
    left join (
        select * from (
            select patient_id, start_date, end_date,
            ROW_NUMBER() over (partition by patient_id order by start_date desc, end_date desc) as rn
            from hef_timespan where name='liver_disease'
        ) as subquery where rn = 1
    ) tls on t0.patient_id = tls.patient_id and tls.start_date < CAST(GETDATE() AS date) and tls.end_date > CAST(GETDATE() AS date)
    left join (
        select * from (
            select patient_id, start_date, end_date,
            ROW_NUMBER() over (partition by patient_id order by start_date desc, end_date desc) as rn
            from hef_timespan where name='rhabdomyolysis'
        ) as subquery where rn = 1
    ) trs on t0.patient_id = trs.patient_id and trs.start_date < CAST(GETDATE() AS date) and trs.end_date > CAST(GETDATE() AS date)
    left join {TT_PRFX}tmp_esr_diag ted on t0.patient_id = ted.patient_id and ted.date > t0.date
    left join {TT_PRFX}tmp_75th_bday t75 on t0.patient_id = t75.patient_id and t75.bd75 < CAST(GETDATE() AS date) and t0.criteria = 'diabetes'
    left join (
        select * from (
            select patient_id, start_date, end_date,
            ROW_NUMBER() over (partition by patient_id order by start_date desc, end_date desc) as rn
            from {TT_PRFX}temp_preg_spans
        ) as subquery where rn = 1
    ) tps on t0.patient_id = tps.patient_id and tps.start_date < CAST(GETDATE() AS date) and tps.end_date > CAST(GETDATE() AS date)
""")
        else:
            cursor.execute(f"""
            select t0.*,
                  case 
                    when t75.bd75 is not null or ted.date is not null 
                      then least(coalesce(t75.bd75, ted.date), coalesce(ted.date, t75.bd75))
                    when tls.start_date is not null or trs.start_date is not null or tps.start_date is not NULL
                      then least(coalesce(tls.start_date, trs.start_date, tps.start_date),
                                coalesce(trs.start_date, tps.start_date, tls.start_date),
                                coalesce(tps.start_date, tls.start_date, trs.start_date))
                    else null::date
                  end as inactive_date,
                  case 
                    when t75.bd75 is not null or ted.date is not null or tls.start_date is not null 
                    or trs.start_date is not null or tps.start_date is not NULL
                      then FALSE
                    else TRUE
                  end as isactive
            into {TT_SYNTX}temp_new_statin_cases
            from    
            (select patient_id, 'statin'::varchar(100) as condition, min(date) as date, string_agg(crit, ',') as criteria,
              'urn:x-esphealth:disease:commonwealth:statin:v1'::varchar(255) as source, 'AR'::varchar(32) as status, null::text as notes,
              null::text as reportables, current_timestamp as created_timestamp, current_timestamp as updated_timestamp, 
              null::timestamp as sent_timestamp, FALSE as followup_sent, 1 as provider_id
            from (select patient_id, crit, min(date) as DATE
                  from {TT_PRFX}temp_statin_case_events
                  group by patient_id, crit) t00
            group by patient_id) t0
            left join (select distinct on (patient_id) patient_id, start_date, end_date 
                      from hef_timespan where name='liver_disease'
                      order by patient_id, start_date desc, end_date desc) tls on t0.patient_id = tls.patient_id and tls.start_date < current_date and tls.end_date > current_date
            left join (select distinct on (patient_id) patient_id, start_date, end_date 
                      from hef_timespan where name='rhabdomyolysis'
                      order by patient_id, start_date desc, end_date desc) trs on t0.patient_id = trs.patient_id and trs.start_date < current_date and trs.end_date > current_date
            left join {TT_PRFX}tmp_esr_diag ted on t0.patient_id = ted.patient_id and ted.date > t0.date
            left join {TT_PRFX}tmp_75th_bday t75 on t0.patient_id = t75.patient_id and t75.bd75 < current_date and t0.criteria = 'diabetes'
            left join (select distinct on (patient_id) patient_id, start_date, end_date 
                      from {TT_PRFX}temp_preg_spans
                      order by patient_id, start_date desc, end_date desc) tps on t0.patient_id = tps.patient_id and tps.start_date < current_date and tps.end_date > current_date
        """)
        # Delete existing statin cases from temp_new_statin_cases if they match conditions in nodis_case
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""delete from {TT_PRFX}temp_new_statin_cases where exists
                            (select 1 FROM nodis_case nc 
                             where nc.patient_id = {TT_PRFX}temp_new_statin_cases.patient_id 
                             AND nc.condition = {TT_PRFX}temp_new_statin_cases.condition 
                             AND nc.date = {TT_PRFX}temp_new_statin_cases.date 
                             AND nc.criteria = {TT_PRFX}temp_new_statin_cases.criteria
                             AND COALESCE(nc.isactive, 1) = COALESCE({TT_PRFX}temp_new_statin_cases.isactive, 1) 
                             AND COALESCE(nc.inactive_date, CAST(GETDATE() as date)) = COALESCE({TT_PRFX}temp_new_statin_cases.inactive_date, CAST(GETDATE() AS date)))""")
        else:
            cursor.execute(f"""delete from {TT_PRFX}temp_new_statin_cases t0 where exists
                            (select null from nodis_case nc 
                             where nc.patient_id = t0.patient_id and nc.condition = t0.condition and nc.date = t0.date 
                               and nc.criteria = t0.criteria
                               and coalesce(nc.isactive, TRUE) = coalesce(t0.isactive, TRUE) 
                               and coalesce(nc.inactive_date, current_date) = coalesce(t0.inactive_date, current_date))""")

        # Identify old statin cases to drop
        cursor.execute(f"""select id into {TT_SYNTX}old_statin_cases_to_drop from nodis_case nc
                          where condition = 'statin' and exists (select null from {TT_PRFX}temp_new_statin_cases sc 
                                                                 where sc.patient_id = nc.patient_id)""")

        # Delete related entries from nodis_caseactivehistory
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""delete from nodis_caseactivehistory 
                            where exists (select 1 FROM {TT_PRFX}old_statin_cases_to_drop ctd 
                            where ctd.id = nodis_caseactivehistory.case_id)""")
        else:
            cursor.execute(f"""delete from nodis_caseactivehistory cah
                            where exists (select null FROM {TT_PRFX}old_statin_cases_to_drop ctd 
                            where ctd.id = cah.case_id)""")
        # Delete related entries from nodis_case_events
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""delete from nodis_case_events 
                            where exists (select 1 from {TT_PRFX}old_statin_cases_to_drop ctd 
                            where ctd.id = nodis_case_events.case_id)""")
        else:
            cursor.execute(f"""delete from nodis_case_events ce where exists (select null from 
                            {TT_PRFX}old_statin_cases_to_drop ctd 
                            where ctd.id = ce.case_id)""")

        # Delete related entries from nodis_case_timespans
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""delete from nodis_case_timespans 
                            where exists (select 1 from {TT_PRFX}old_statin_cases_to_drop ctd 
                            where ctd.id = nodis_case_timespans.case_id)""")
        else:
            cursor.execute(f"""delete from nodis_case_timespans cts 
                            where exists (select null from {TT_PRFX}old_statin_cases_to_drop ctd 
                            where ctd.id = cts.case_id)""")
         # Delete old statin cases from nodis_case
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""delete from nodis_case 
                            where exists 
                              (select 1 from {TT_PRFX}old_statin_cases_to_drop ctd where ctd.id = nodis_case.id)""")
        else:
            cursor.execute(f"""delete from nodis_case c 
                            where exists (select null from {TT_PRFX}old_statin_cases_to_drop ctd where ctd.id = c.id)""")

        # Insert new statin cases into nodis_case
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""insert into nodis_case (patient_id, condition, date, criteria, source, status, notes, 
                                reportables, created_timestamp, updated_timestamp, 
                                sent_timestamp, followup_sent, 
                                provider_id, inactive_date, isactive)
                              select patient_id, condition, date, criteria, source, status, notes, reportables,
                                cast(created_timestamp as datetime2), cast(updated_timestamp as datetime2), 
                                cast(sent_timestamp as datetime2), followup_sent, provider_id, inactive_date, isactive 
                              from {TT_PRFX}temp_new_statin_cases""")
        else:
            cursor.execute(f"""insert into nodis_case (patient_id, condition, date, criteria, source, status, notes, 
                              reportables, created_timestamp, updated_timestamp, sent_timestamp, followup_sent, provider_id, 
                              inactive_date, isactive)
                              select * from {TT_PRFX}temp_new_statin_cases""")

        # Select new statin case IDs into a temporary table
        cursor.execute(f"""select patient_id, id as case_id 
                           into {TT_SYNTX}tmp_statin_case_ids
                           from nodis_case c 
                           where condition = 'statin'
                             and exists (select null from {TT_PRFX}temp_new_statin_cases sc where c.patient_id = sc.patient_id)""")

        # Insert new statin case events into nodis_case_events
        cursor.execute(f"""insert into nodis_case_events (event_id, case_id)
                           select * 
                           from (select ted.event_id, sci.case_id 
                                 from {TT_PRFX}tmp_esr_diag ted
                                 join {TT_PRFX}tmp_statin_case_ids sci on sci.patient_id = ted.patient_id
                                 union 
                                 select sce.id as event_id, sci.case_id
                                 from {TT_PRFX}temp_statin_case_events sce
                                 join {TT_PRFX}tmp_statin_case_ids sci on sci.patient_id = sce.patient_id where sce.crit <> 'diabetes') t0
                           where not exists (select null from nodis_case_events nce 
                                             where nce.event_id = t0.event_id and nce.case_id = t0.case_id)""")
        # Creating temporary tables for rhabdomyolysis, liver disease, and statin timespans
        cursor.execute(f"""select id, patient_id, start_date, end_date, pattern
                           into {TT_SYNTX}temp_rhabdo_span_ids
                           from hef_timespan
                           where name = 'rhabdomyolysis'""")

        cursor.execute(f"""select id, patient_id, start_date, end_date, pattern
                           into {TT_SYNTX}temp_liver_span_ids
                           from hef_timespan
                           where name = 'liver_disease'""")

        cursor.execute(f"""select id, patient_id, start_date, end_date, pattern
                           into {TT_SYNTX}temp_hef_rx_spans
                           from hef_timespan
                           where name = 'statin'""")

        # Inserting data into nodis_case_timespans
        cursor.execute(f"""insert into nodis_case_timespans (timespan_id, case_id)
                           select t0.timespan_id, t1.case_id
                           from (select distinct id as timespan_id, patient_id 
                                 from {TT_PRFX}temp_rhabdo_span_ids
                                 union 
                                 select distinct timespan_id, patient_id 
                                 from {TT_PRFX}temp_preg_spans
                                 union 
                                 select distinct id as timespan_id, patient_id 
                                 from {TT_PRFX}temp_liver_span_ids
                                 union 
                                 select distinct id as timespan_id, patient_id 
                                 from {TT_PRFX}temp_hef_rx_spans) t0
                           join {TT_PRFX}tmp_statin_case_ids t1 on t1.patient_id = t0.patient_id""")

#history values: First character represents statin prescription status.
#  H for currently on High intensity statin, 
#  O for currently on Other Statin therapy, 
#  N for no statin prescription,
#  C for currently medically Contraindicated
#  D for deactivated 
#  For the first 3 above, you must have a Second character
#  A for ASCVD
#  H for hyperlipidemia
#  D for diabetic
#  I'll add an "_" character for the deactivated and contraindicated groups.
#  So, a total of 9 2-character active disease status values, plus D_ for deactivated or C_ for contraindicated
#  Critiria objects have a heirarchy.  Diabetes can only occur if there is not hyperlipid or ASCVD condition
#    Hyperlipid can only occur if there is no ASCVD condition
#    As soon as there is ASCVD, it takes over.
        
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
          cursor.execute(f"""
              select * into {TT_SYNTX}statin_criteria_objs from (
                  select patient_id, date as start_date, id as object_id, content_type_id, crit as htype
                  from {TT_PRFX}temp_statin_case_events t0
                  where crit = 'diabetes' 
                    and not exists (select null from {TT_PRFX}temp_statin_case_events t1 
                                    where t0.patient_id = t1.patient_id and t0.date >= t1.date
                                      and t1.crit <> 'diabetes')
                  union 
                  select patient_id, date as start_date, id as object_id, content_type_id, crit as htype
                  from {TT_PRFX}temp_statin_case_events t0
                  where crit = 'ldl>=190' 
                    and not exists (select null from {TT_PRFX}temp_statin_case_events t1 
                                    where t0.patient_id = t1.patient_id and t0.date >= t1.date
                                      and t1.crit = 'ASCVD')
                  union
                  select patient_id, date as start_date, id as object_id, content_type_id, crit as htype
                  from {TT_PRFX}temp_statin_case_events t0
                  where crit = 'ASCVD'
              ) t00
          """)
        else:
          cursor.execute(f"""
            select * into {TT_SYNTX}statin_criteria_objs from (
                select patient_id, date as start_date, id as object_id, content_type_id, crit as htype
                from {TT_PRFX}temp_statin_case_events t0
                where crit = 'diabetes' 
                  and not exists (select null from {TT_PRFX}temp_statin_case_events t1 
                                  where t0.patient_id = t1.patient_id and t0.date >= t1.date
                                    and t1.crit <> 'diabetes')
                union 
                select patient_id, date as start_date, id as object_id, content_type_id, crit as htype
                from {TT_PRFX}temp_statin_case_events t0
                where crit = 'ldl>=190' 
                  and not exists (select null from {TT_PRFX}temp_statin_case_events t1 
                                  where t0.patient_id = t1.patient_id and t0.date >= t1.date
                                    and t1.crit = 'ASCVD')
                union
                select patient_id, date as start_date, id as object_id, content_type_id, crit as htype
                from {TT_PRFX}temp_statin_case_events t0
                where crit = 'ASCVD'
            ) t00
        """)
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""select *, 
                                coalesce(lead(start_date) over (partition by patient_id, htype order by start_date, object_id),
                                cast(getdate() as date)) as end_date,
                                row_number() over (partition by patient_id, htype order by start_date, object_id) as rownum
                          into {TT_SYNTX}statin_criteria_objs_augmnted
                          from {TT_PRFX}statin_criteria_objs""")
        else:
            cursor.execute(f"""select *, 
                                coalesce(lead(start_date) over (partition by patient_id, htype order by start_date, object_id),
                                current_date) as end_date,
                                row_number() over (partition by patient_id, htype order by start_date, object_id) as rownum
                          into {TT_SYNTX}statin_criteria_objs_augmnted
                          from {TT_PRFX}statin_criteria_objs""")
        cursor.execute(f"""with t0 as 
                               (select patient_id, min(start_date) as start_date, max(end_date) as end_date, 
                                       max(rownum) as rownum, htype
                                from {TT_PRFX}statin_criteria_objs_augmnted
                                group by patient_id, htype),
                            t1 as (select patient_id, start_date, end_date, rownum, htype,
                                     lead(start_date) over (partition by patient_id order by start_date) as next_start
                                   from t0)
                          select t1.patient_id, t1.htype, t1.start_date, 
                              case when t1.end_date > t1.next_start then t1.next_start
                                   else t1.end_date end as end_date, 
                              t2.content_type_id, t2.object_id
                          into {TT_SYNTX}statin_criteria_history
                          from t1 
                          join {TT_PRFX}statin_criteria_objs_augmnted t2 
                            on t2.patient_id=t1.patient_id and t2.htype=t1.htype and t1.rownum=t2.rownum""")                       
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""
                SELECT * INTO {TT_SYNTX}statin_contra_objs FROM (
                    SELECT patient_id, start_date, end_date, 
                          rs.id AS object_id, ct.id AS content_type_id, CAST('rhabdo' AS varchar(10)) AS htype 
                    FROM {TT_PRFX}temp_rhabdo_span_ids rs
                    JOIN django_content_type ct ON ct.app_label='hef' AND ct.model='timespan'
                    UNION 
                    SELECT patient_id, start_date, end_date, 
                          timespan_id AS object_id, ct.id AS content_type_id, CAST('preg' AS varchar(10)) AS htype 
                    FROM {TT_PRFX}temp_preg_spans
                    JOIN django_content_type ct ON ct.app_label='hef' AND ct.model='timespan'
                    UNION 
                    SELECT patient_id, start_date, end_date, 
                          ls.id AS object_id, ct.id AS content_type_id, CAST('liver' AS varchar(10)) AS htype 
                    FROM {TT_PRFX}temp_liver_span_ids ls
                    JOIN django_content_type ct ON ct.app_label='hef' AND ct.model='timespan'
                ) t0
                """)
        else:
            cursor.execute(f"""select * into {TT_SYNTX}statin_contra_objs from (
                          select patient_id, start_date, end_date, 
                              rs.id as object_id, ct.id as content_type_id, 'rhabdo'::varchar(10) as htype 
                          from {TT_PRFX}temp_rhabdo_span_ids rs
                          join django_content_type ct on app_label='hef' and model='timespan'
                          union 
                          select patient_id, start_date, end_date, 
                              timespan_id as object_id, ct.id as content_type_id, 'preg'::varchar(10) as htype 
                          from {TT_PRFX}temp_preg_spans
                          join django_content_type ct on app_label='hef' and model='timespan'
                          union 
                          select patient_id, start_date, end_date, 
                              ls.id as object_id, ct.id as content_type_id, 'liver'::varchar(10) as htype 
                          from {TT_PRFX}temp_liver_span_ids ls
                          join django_content_type ct on app_label='hef' and model='timespan') t0""")
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""WITH contra_island_starts AS 
                        (SELECT *, CASE WHEN DATEDIFF(day, LAG(end_date) OVER (PARTITION BY patient_id ORDER BY start_date, end_date, 
                        object_id), start_date) IS NULL THEN 1 WHEN DATEDIFF(day, LAG(end_date) OVER (PARTITION BY patient_id ORDER BY 
                        start_date, end_date, object_id), start_date) 
                                > 0 THEN 1
                                 ELSE 0
                            END island_start,
                            ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY start_date, end_date, object_id) rownumber
                         FROM {TT_PRFX}statin_contra_objs),
                    contra_island_grps AS
                        (SELECT *, SUM(island_start) OVER 
                                   (PARTITION BY patient_id ORDER BY start_date, end_date, object_id) AS island
                         FROM contra_island_starts),
                    contra_islands AS
                        (SELECT patient_id, MIN(start_date) AS start_date, 
                             MAX(COALESCE(end_date, CAST(GETDATE() AS DATE))) AS end_date,
                             MAX(rownumber) AS maxrow
                         FROM contra_island_grps
                         GROUP BY patient_id, island)
                  SELECT t0.patient_id, t0.start_date, CAST(t0.end_date AS DATE) AS end_date, t1.object_id, t1.content_type_id, t1.htype
                  INTO {TT_SYNTX}contra_islands_last_object
                  FROM contra_islands t0 
                  JOIN contra_island_grps t1 ON t1.patient_id=t0.patient_id AND t1.rownumber=t0.maxrow""")
        else:
            cursor.execute(f"""with contra_island_starts as 
                            (select *, 
                                case when date_part('day',start_date-lag(end_date) 
                                                       over (partition by patient_id order by start_date, end_date, object_id))  
                                                       is null then 1
                                     when date_part('day',start_date-lag(end_date) 
                                                       over (partition by patient_id order by start_date, end_date, object_id)) 
                                                       > 0 then 1
                                     else 0
                                end island_start,
                                row_number() over (partition by patient_id order by start_date, end_date, object_id) rownumber
                             from {TT_PRFX}statin_contra_objs),
                            contra_island_grps as
                            (select *, sum(island_start) over 
                                       (partition by patient_id order by start_date, end_date, object_id) as island
                             from contra_island_starts),
                            contra_islands as
                            (select patient_id, min(start_date) as start_date, 
                                 max(coalesce(end_date,current_date)) end_date,
                                 max(rownumber) maxrow
                             from contra_island_grps
                             group by patient_id, island)
                          select t0.patient_id, t0.start_date, t0.end_date::date, t1.object_id, t1.content_type_id, t1.htype
                          into {TT_SYNTX}contra_islands_last_object
                          from contra_islands t0 
                          join contra_island_grps t1 on t1.patient_id=t0.patient_id and t1.rownumber=t0.maxrow""")  
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""
                          SELECT * INTO {TT_SYNTX}deactivate 
                          FROM (
                              SELECT patient_id, date AS start_date, CAST(NULL AS DATE) AS end_date, 
                                  event_id AS object_id, ct.id AS content_type_id, 'esr' AS htype 
                              FROM {TT_PRFX}tmp_esr_diag
                              JOIN django_content_type ct ON ct.app_label = 'hef' AND ct.model = 'event'
                              UNION 
                              SELECT patient_id, bd75 AS start_date, CAST(NULL AS DATE) AS end_date, 
                                  patient_id AS object_id, ct.id AS content_type_id, '75th' AS htype
                              FROM {TT_PRFX}tmp_75th_bday t75
                              JOIN django_content_type ct ON ct.app_label = 'emr' AND ct.model = 'patient'
                              WHERE bd75 <= CAST(GETDATE() AS DATE) 
                              AND EXISTS (
                                  SELECT 1 FROM {TT_PRFX}statin_criteria_history sch 
                                  WHERE sch.htype = 'diabetes' 
                                  AND sch.patient_id = t75.patient_id
                                  AND t75.bd75 BETWEEN sch.start_date AND sch.end_date
                              )
                          ) t0
                          """)
        else:
          cursor.execute(f"""select * into {TT_SYNTX}deactivate 
                          from 
                            (select patient_id, date as start_date, null::date as end_date, 
                               event_id as object_id, ct.id as content_type_id, 'esr'::varchar(10) as htype 
                             from {TT_PRFX}tmp_esr_diag
                             join django_content_type ct on app_label='hef' and model='event'
                             union 
                             select patient_id, bd75 as start_date, null::date as end_date, 
                                 patient_id as object_id, ct.id as content_type_id, '75th'::varchar(10) as htype
                             from {TT_PRFX}tmp_75th_bday t75
                             join django_content_type ct on app_label='emr' and model='patient'
                             where bd75 <=current_date and exists (select null from {TT_PRFX}statin_criteria_history sch 
                                                                where sch.htype = 'diabetes' 
                                                                      and sch.patient_id=t75.patient_id
                                                                      and t75.bd75 between sch.start_date and sch.end_date)) t0""")
        cursor.execute(f"""select patient_id, start_date, end_date, rx.id as object_id, ct.id as content_type_id, pattern as htype 
                          into {TT_SYNTX}statin_rx_obj
                          from {TT_PRFX}temp_hef_rx_spans rx
                          join django_content_type ct on app_label='hef' and model='timespan'""")
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
            cursor.execute(f"""WITH statin_island_starts AS 
                        (SELECT *, 
                            CASE 
                                WHEN DATEDIFF(day, LAG(end_date) OVER (PARTITION BY patient_id, htype ORDER BY start_date, end_date,
                                 object_id), start_date) IS NULL THEN 1
                                WHEN DATEDIFF(day, LAG(end_date) OVER (PARTITION BY patient_id, htype ORDER BY start_date, end_date,
                                 object_id), start_date) > 0 THEN 1
                                ELSE 0
                            END AS island_start,
                            ROW_NUMBER() OVER (PARTITION BY patient_id, htype ORDER BY start_date, end_date, object_id) AS rownumber
                         FROM {TT_PRFX}statin_rx_obj),
                    statin_island_grps AS
                        (SELECT *, SUM(island_start) OVER 
                                   (PARTITION BY patient_id, htype ORDER BY start_date, end_date, object_id) AS island
                         FROM statin_island_starts),
                    statin_islands AS
                        (SELECT patient_id, htype, MIN(start_date) AS start_date, 
                             MAX(COALESCE(end_date, CAST(GETDATE() AS DATE))) AS end_date,
                             MAX(rownumber) AS maxrow
                         FROM statin_island_grps
                         GROUP BY patient_id, htype, island)
                    SELECT t0.patient_id, t0.htype, t0.start_date, t0.end_date, t1.object_id, t1.content_type_id,
                        LEAD(t0.start_date) OVER (PARTITION BY t0.patient_id ORDER BY t0.start_date, t0.end_date) AS next_start
                    INTO {TT_SYNTX}statin_islands_last_object
                    FROM statin_islands t0 
                    JOIN statin_island_grps t1 ON t1.patient_id = t0.patient_id AND t1.htype = t0.htype 
                        AND t1.rownumber = t0.maxrow""")
        else:
            cursor.execute(f"""with statin_island_starts as 
                            (select *, 
                               case when start_date-lag(end_date) over 
                                          (partition by patient_id, htype order by start_date, end_date, object_id) 
                                      is null then 1
                                    when start_date-lag(end_date) over 
                                          (partition by patient_id, htype order by start_date, end_date, object_id) 
                                      > 0 then 1
                                    else 0
                               end island_start,
                               row_number() over (partition by patient_id, htype order by start_date, end_date, object_id) rownumber
                             from {TT_PRFX}statin_rx_obj),
                            statin_island_grps as
                            (select *, sum(island_start) over 
                                       (partition by patient_id, htype order by start_date, end_date, object_id) as island
                             from statin_island_starts),
                            statin_islands as
                            (select patient_id, htype, min(start_date) as start_date, 
                                 max(coalesce(end_date,current_date)) end_date,
                                 max(rownumber) maxrow
                             from statin_island_grps
                             group by patient_id, htype, island)
                          select t0.patient_id, t0.start_date, t0.end_date, t1.object_id, t1.content_type_id, t1.htype,
                             lead(t0.start_date) over (partition by t0.patient_id order by t0.start_date, t0.end_date) as next_start
                          into {TT_SYNTX}statin_islands_last_object
                          from statin_islands t0 
                          join statin_island_grps t1 on t1.patient_id=t0.patient_id and t1.htype=t0.htype 
                            and t1.rownumber=t0.maxrow""")
        cursor.execute(f"""select patient_id, start_date, 
                               case when end_date>next_start then next_start else end_date end as end_date, 
                               object_id, content_type_id, htype 
                               into {TT_SYNTX}statin_islands 
                               from {TT_PRFX}statin_islands_last_object""")
        cursor.execute(f"""with t0 as (select patient_id, start_date, end_date, object_id, content_type_id, htype from 
                            {TT_PRFX}deactivate 
                            union 
                            select patient_id, start_date, end_date, object_id, content_type_id, htype from 
                            {TT_PRFX}statin_criteria_history),
                                t1 as (select t0.*, lag(htype) over (partition by patient_id order by start_date, htype desc) 
                                as prior_htype from t0),
                                t2 as (select patient_id, start_date, end_date, object_id, content_type_id, htype,
                                          lead(htype) over (partition by patient_id order by start_date, htype desc) as next_htype
                                from t1 
                                    where (htype <> '75th' or (htype='75th' and prior_htype='diabetes')) 
                                    and not exists (select null from t0 
                                                    where t0.patient_id=t1.patient_id and t0.htype='esr' 
                                                    and t1.htype<>'esr' and t1.start_date>=t0.start_date))
                                select patient_id, start_date, 
                                case when next_htype in ('bd75','esr') 
                                    then lead(start_date) over (partition by patient_id order by start_date, htype desc)
                                else end_date end as end_date,
                                object_id, content_type_id, htype
                                into {TT_SYNTX}criteria_deactivated
                                from t2""")
        cursor.execute(f"""with t0 as (select patient_id, start_date, end_date, object_id, content_type_id, htype, 1 as source from 
                            {TT_PRFX}criteria_deactivated
                            union
                            select patient_id, start_date, end_date, object_id, content_type_id, htype, 2 as source from
                            {TT_PRFX}contra_islands_last_object),
                                t1 as (select *, lag(source) over (partition by patient_id order by start_date) as prior_source,
                                                lag(end_date) over (partition by patient_id order by start_date) as prior_end,
                                                lead(source) over (partition by patient_id order by start_date) as next_source,
                                                lead(start_date) over  (partition by patient_id order by start_date) as next_start
                                from t0 
                                      where not exists (select null from t0 as t00 
                                                        where t00.patient_id=t0.patient_id and t00.source=2 and t0.source=1
                                                           and t0.start_date >= t00.start_date and t0.end_date<=t00.end_date))
                                select patient_id, 
                                 case when prior_source=2 and source=1 and prior_end>start_date then prior_end else start_date end 
                                    as start_date, case when next_source=2 and source=1 and next_start<end_date then next_start else 
                                    end_date end as end_date,
                                 object_id, content_type_id, htype
                            into {TT_SYNTX}crit_deacted_contraind
                            from t1""")
        cursor.execute(f"""with t0 as (select t00.patient_id, t00.start_date, t00.end_date, t00.object_id, t00.content_type_id, t00.htype,
                                             t01.htype as last_type
                                      from {TT_PRFX}crit_deacted_contraind t00 left join {TT_PRFX}statin_islands t01
                                        on t00.patient_id=t01.patient_id and t00.start_date>= t01.start_date
                                      and not exists (select null from {TT_PRFX}statin_islands t010 
                                                        where t010.patient_id=t01.patient_id and t010.start_date<t01.start_date)
                                      union
                                      select t00.patient_id, t00.start_date, t00.end_date, t00.object_id, t00.content_type_id, t00.htype,
                                             t01.htype as last_type
                                      from {TT_PRFX}statin_islands t00 join {TT_PRFX}crit_deacted_contraind t01
                                        on t00.patient_id=t01.patient_id and t00.start_date>= t01.start_date
                                        and not exists (select null from {TT_PRFX}crit_deacted_contraind t010 
                                                        where t010.patient_id=t01.patient_id and t010.start_date<t01.start_date)),
                               t1 as (select cid.case_id, 
                                        case when htype = 'ASCVD' and last_type is null then 'NA'
                                             when htype = 'ASCVD' and last_type = 'intensity:m' then 'OA'
                                             when htype = 'ASCVD' and last_type = 'intensity:h' then 'HA'
                                             when htype = 'ldl>=190' and last_type is null then 'NH'
                                             when htype = 'ldl>=190' and last_type = 'intensity:m' then 'OH'
                                             when htype = 'ldl>=190' and last_type = 'intensity:h' then 'HH'
                                             when htype = 'diabetes' and last_type is null then 'ND'
                                             when htype = 'diabetes' and last_type = 'intensity:m' then 'OD'
                                             when htype = 'diabetes' and last_type = 'intensity:h' then 'HD'
                                             when last_type = 'ASCVD' and htype is null then 'NA'
                                             when last_type = 'ASCVD' and htype = 'intensity:m' then 'OA'
                                             when last_type = 'ASCVD' and htype = 'intensity:h' then 'HA'
                                             when last_type = 'ldl>=190' and htype is null then 'NH'
                                             when last_type = 'ldl>=190' and htype = 'intensity:m' then 'OH'
                                             when last_type = 'ldl>=190' and htype = 'intensity:h' then 'HH'
                                             when last_type = 'diabetes' and htype is null then 'ND'
                                             when last_type = 'diabetes' and htype = 'intensity:m' then 'OD'
                                             when last_type = 'diabetes' and htype = 'intensity:h' then 'HD'
                                             when htype in ('liver','rhabdo','preg') then 'C_'
                                             when htype in ('esr','75th') then 'D_' 
                                             else null end as status, 
                                        start_date as date, '' as change_reason, start_date as latest_event_date,
                                        object_id, content_type_id
                              , htype, last_type
                              from t0 join {TT_PRFX}tmp_statin_case_ids cid on t0.patient_id=cid.patient_id)
                          select * into {TT_SYNTX}statin_caseactivehistory from t1 
                              where not exists (select null from t1 as t01 
                                                where t1.case_id=t01.case_id 
                                                   and htype like 'instensity%' and last_type in ('esr','rhabdo','preg','75th','liver'))
                                    and not exists (select null from t1 as t02
                                                    where t1.case_id=t02.case_id and t1.date=t02.date
                                                          and t1.htype like 'intensity%')
                                    and status is not null""")
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc':
                cursor.execute(f"""
                    WITH RankedCases AS (
                        SELECT
                            status,
                            date,
                            change_reason,
                            latest_event_date,
                            object_id,
                            case_id,
                            content_type_id,
                            ROW_NUMBER() OVER (
                                PARTITION BY case_id, date 
                                ORDER BY 
                                    CASE status
                                        WHEN 'D_' THEN 1
                                        WHEN 'C_' THEN 2
                                        WHEN 'HA' THEN 3
                                        WHEN 'HH' THEN 4
                                        WHEN 'HD' THEN 5
                                        WHEN 'OA' THEN 6
                                        WHEN 'OH' THEN 7
                                        WHEN 'OD' THEN 8 
                                        WHEN 'NA' THEN 9
                                        WHEN 'NH' THEN 10
                                        WHEN 'ND' THEN 11
                                        ELSE 12
                                    END
                            ) AS rn
                        FROM {TT_PRFX}statin_caseactivehistory
                    )
                    INSERT INTO nodis_caseactivehistory (
                        status, date, change_reason, latest_event_date, 
                        object_id, case_id, content_type_id
                    )
                    SELECT
                        status,
                        date,
                        change_reason,
                        latest_event_date,
                        object_id,
                        case_id,
                        content_type_id
                    FROM RankedCases
                    WHERE rn = 1
                    """)
        else:
                cursor.execute(f"""insert into nodis_caseactivehistory (status, date, change_reason, latest_event_date, object_id,
                                    case_id, content_type_id)
                                    select distinct on (case_id, date) status, date, change_reason, latest_event_date, object_id,
                                    case_id, content_type_id
                                from {TT_PRFX}statin_caseactivehistory order by case_id, date,
                               case status when 'D_' then 1 when 'C_' then 2 when 'HA' then 3 when 'HH' then 4
                                 when 'HD' then 5 when 'OA' then 6 when 'OH' then 7 when 'OD' then 8 
                                 when 'NA' then 9 when 'NH' then 10 when 'ND' then 11 end""")
        return cursor.rowcount   
