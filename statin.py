'''
                                 ESP Health
                        Notifiable Diseases Framework
                         Statin Therapy Case Generator


@authors: Jeff Andre <jandre@commoninf.com>, Bob Zambarano <bzambarano@commoninf.com>
@organization: commonwealth informatics https://www.commoninf.com
@contact: https://www.esphealth.org
@copyright: (c) 2021 Commonwealth Informatics, Inc.
@license: LGPL
'''
import pdb
import datetime 
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.db.models import F
from django.db.models.functions import Coalesce
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from ESP.utils import log
from ESP.hef.base import Event
from ESP.hef.base import LabResultFixedThresholdHeuristic, HEF_CORE_URI, BaseTimespanHeuristic
from ESP.hef.base import PrescriptionHeuristic, DiagnosisHeuristic, BaseEventHeuristic
from ESP.hef.base import Dx_CodeQuery
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case, CaseActiveHistory
from ESP.static.models import DrugSynonym
from ESP.conf.models import LabTestMap
from ESP.hef.models import Timespan
from ESP.emr.models import Prescription
from django.contrib.contenttypes.models import ContentType

from statin_sql_utils import generate_statin_events, generate_statin_timespans, generate_liver_events, generate_liver_timespans, generate_rhabdo_events
from statin_sql_utils import generate_rhabdo_timespans, generate_lx_hi_chol_events, generate_ascvd_events, generate_statin_cases 

class statinRxHeuristic(BaseEventHeuristic):
    '''
    A sql based rx heuristic
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name
    
    uri = u'urn:x-esphealth:heuristic:commoninf:rx:statin:v1'

    def __hash__(self):
        return hash(self.name)
    
    @property
    def short_name(self):
        return u'rx:%s' % self.name

    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]

    @property
    def rx_event_name(self):
        return 'prescription:%s' % self.name 

    @property
    def event_names(self):
        return [self.rx_event_name]

    def generate(self):
        counter = generate_statin_events(self.uri)
        log.info('Generated %s new events for %s' % (counter, self))
        return counter
        

class ldlLxHeuristic(BaseEventHeuristic):
    '''
    A sql based lx heuristic
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name
    
    uri = u'urn:x-esphealth:heuristic:commoninf:lx:cholesterol-ldl:threshold:gte:190:v1'

    def __hash__(self):
        return hash(self.name)
    
    @property
    def short_name(self):
        return u'lx:%s' % self.name

    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]

    @property
    def lx_event_name(self):
        return 'labresult:%s' % self.name 

    @property
    def event_names(self):
        return [self.lx_event_name]

    def generate(self):
        counter = generate_lx_hi_chol_events(self.uri)
        log.info('Generated %s new events for %s' % (counter, self))
        return counter
        

class liverDiseaseDxHeuristic(BaseEventHeuristic):
    '''
    A sql based dx heuristic
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name
        
    uri = u'urn:x-esphealth:heuristic:commoninf:dx:liverdisease:v1'

    def __hash__(self):
        return hash(self.name)
    
    @property
    def short_name(self):
        return u'dx:%s' % self.name

    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]

    @property
    def dx_event_name(self):
        return 'diagnosis:%s' % self.name 

    @property
    def event_names(self):
        return [self.dx_event_name]

    def generate(self):
        counter = generate_liver_events(self.uri)
        log.info('Generated %s new events for %s' % (counter, self))
        return counter
        

class rhabdoDxHeuristic(BaseEventHeuristic):
    '''
    A sql based dx heuristic
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name
        
    uri = u'urn:x-esphealth:heuristic:commoninf:dx:rhabdomyolysis:v1'

    def __hash__(self):
        return hash(self.name)
    
    @property
    def short_name(self):
        return u'dx:%s' % self.name

    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]

    @property
    def dx_event_name(self):
        return 'diagnosis:%s' % self.name 

    @property
    def event_names(self):
        return [self.dx_event_name]

    def generate(self):
        counter = generate_rhabdo_events(self.uri)
        log.info('Generated %s new events for %s' % (counter, self))
        return counter
        
class ascvdDxHeuristic(BaseEventHeuristic):
    '''
    A sql based dx heuristic
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name
    
    uri = u'urn:x-esphealth:heuristic:commoninf:dx:ascvd:v1'

    def __hash__(self):
        return hash(self.name)
    
    @property
    def short_name(self):
        return u'dx:%s' % self.name

    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]

    @property
    def dx_event_name(self):
        return 'diagnosis:%s' % self.name 

    @property
    def event_names(self):
        return [self.dx_event_name]

    def generate(self):
        counter = generate_ascvd_events(self.uri)
        log.info('Generated %s new events for %s' % (counter, self))
        return counter
    
class statinTimespanHeuristic(BaseTimespanHeuristic):
    '''
    A sql based timespan heuristic
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name
        
    uri = u'urn:x-esphealth:heuristic:commoninf:timespan:statin:v1'
        
    def __hash__(self):
        return hash(self.name)
        
    @property
    def short_name(self):
        return 'timespan:statin'

    core_uris = ['urn:x-esphealth:hef:core:v1']

    @property
    def timespan_names(self):
        return ['statin', ]
        
    @property
    def event_heuristics(self):
        return []

    def generate(self):
        counter = generate_statin_timespans(self.uri)
        log.info('Generated %s new timespans for %s' % (counter, self.name))
        return counter
    
class liverdiseaseTimespanHeuristic(BaseTimespanHeuristic):
    '''
    A sql based timespan heuristic
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name
        
    uri = u'urn:x-esphealth:heuristic:commoninf:timespan:liver_disease:v1'
        
    def __hash__(self):
        return hash(self.name)
        
    @property
    def short_name(self):
        return 'timespan:liver_disease'

    core_uris = ['urn:x-esphealth:hef:core:v1']

    @property
    def timespan_names(self):
        return ['liver_disease', ]

    @property
    def event_heuristics(self):
        return []

    def generate(self):
        counter = generate_liver_timespans(self.uri)
        log.info('Generated %s new timespans for %s' % (counter, self.name))
        return counter
    
class rhabdoTimespanHeuristic(BaseTimespanHeuristic):
    '''
    A sql based timespan heuristic
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name
    
    uri = u'urn:x-esphealth:heuristic:commoninf:timespan:rhabdomyolysis:v1'
        
    def __hash__(self):
        return hash(self.name)
        
    @property
    def short_name(self):
        return 'timespan:rhabdomyolysis'

    core_uris = ['urn:x-esphealth:hef:core:v1']

    @property
    def timespan_names(self):
        return ['rhabdomyolysis', ]

    @property
    def event_heuristics(self):
        return []

    def generate(self):
        counter = generate_rhabdo_timespans(self.uri)
        log.info('Generated %s new timespans for %s' % (counter, self.name))
        return counter


class Statin(DiseaseDefinition):
    '''
    Statin Therapy
    '''
    
    conditions = ['statin']
    
    uri = 'urn:x-esphealth:disease:commoninf:statin:v1'
    
    short_name = 'statin'
    
    test_name_search_strings = ['statin',]

    # statin rx event names
    rx_statin_high = [
            'rx:atorvastatin',
            'rx:rosuvastatin',
        ]
    rx_statin_med = [
            'rx:fluvastatin',
            'rx:lovastatin',
            'rx:pitavastatin',
            'rx:pravastatin',
            'rx:simvastatin',
            'rx:amlodipine',
        ]
    # dx event names
    dx_esrenal = ['dx:esrenal_disease']
    dx_event_names = [
            'dx:liver_disease',
            'dx:endstagerenal',
            'dx:rhabdomyolysis'
            ]
    # diabetes case names
    diabetes_case_names = ['diabetes:type-1','diabetes:type-2']

    criteria_a_event_name = ['dx:ascvd']
    criteria_b_event_names = ['lx:cholesterol-ldl:threshold:gte:190']

    criteria_names ={'1':'Criteria a: High Risk Group 1: ASCVD',
                     '2':'Criteria b: High Risk Group 2: Hypercholesterolemia',
                     '3':'Criteria c: High Risk Group 3: Diabetes'}

    @property
    def event_heuristics(self):
        heuristic_list = []
        
        """ Diagnosis Codes
        """
        heuristic_list.append( ascvdDxHeuristic(
            name = 'ascvd'))
        heuristic_list.append( rhabdoDxHeuristic(
            name = 'rhabdomyolysis'))
        heuristic_list.append( liverDiseaseDxHeuristic(
                name = 'liver_disease'))
        """ Prescriptions
        """
        heuristic_list.append( statinRxHeuristic(
                name = 'statin'))
                
        """ Lab Results
        """
        heuristic_list.append( ldlLxHeuristic(
                name = 'cholesterol-ldl:threshold:gte:190'))

        return heuristic_list

    @property
    def timespan_heuristics(self):
        heuristic_list = []
        
        heuristic_list.append( statinTimespanHeuristic( 
                name = 'statin'))
        heuristic_list.append( liverdiseaseTimespanHeuristic(
                name ='liver_disease'))
        heuristic_list.append( rhabdoTimespanHeuristic(
                name ='rhabdomyolysis'))
        return heuristic_list

    def generate(self):
        log.info('Generating cases of %s' % self.short_name)
        counter = generate_statin_cases()
        return counter # Count of new cases
                
#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------


def event_heuristics():
    return Statin().event_heuristics

def timespan_heuristics():
    return Statin().timespan_heuristics

def disease_definitions():
    return [Statin()]

