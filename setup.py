'''
                                  ESP Health
                                Statin Therapy
                             Packaging Information


@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: Commonwealth Informatics https://commoninf.com
@contact: https://www.esphealth.org
@copyright: (c) 2021
@license: LGPL 3.0 http://www.gnu.org/licenses/lgpl-3.0.txt
'''

from setuptools import find_packages
from setuptools import setup

setup(
    name='esp-plugin_statin',
    version='1.1',
    author='Bob Zambarano',
    author_email='bzambarano@commoninf.com',
    description='Statin Therapy definition module for ESP Health application',
    license='LGPLv3',
    keywords='Statin Therapy algorithm disease surveillance public health epidemiology',
    url='https://esphealth.org',
    packages=find_packages(exclude=['ez_setup']),
    install_requires=[
    ],
    entry_points='''
        [esphealth]
        disease_definitions = statin:disease_definitions
        event_heuristics = statin:event_heuristics
        timespan_heuristics = statin:timespan_heuristics
    '''
)
